var CROSS = "X";
var ZERO = "O";
var EMPTY = " ";
let DIMENSION = undefined;
let TurnCounter = 0;
let Player = CROSS;
let Game = 1;
let FieldArray;



startGame();

// выделение памяти под массив с полем игры

function iniField() {
    FieldArray = new Array(DIMENSION);
    for (let i = 0; i < FieldArray.length; i++) {
        FieldArray[i] = new Array(DIMENSION);
    }    
}
function checkRows() {
  
  let counter = 0;
  for (let i = 0; i < FieldArray.length; i++) {
    for (let j = 0; j < FieldArray.length; j++) {
      if (FieldArray[i][j] === Player) {
        counter++;
      }      
    }
    if (counter === FieldArray.length) {
      for (let k = 0; k < FieldArray.length; k++) {
        renderSymbolInCell(Player, i, k, 'red'); 
      }
      return true;
    }
    else {
      counter = 0;
    }
  }
  return false;
}
function checkColumns() { 
  let counter = 0;
  for (let i = 0; i < FieldArray.length; i++) {
    for (let j = 0; j < FieldArray.length; j++) {
      if (FieldArray[j][i] === Player) {
        counter++;
      }      
    }
    if (counter === FieldArray.length) {
      for (let k = 0; k < FieldArray.length; k++) {
        renderSymbolInCell(Player, k, i, 'red'); 
      }
      return true;
    }
    else {
      counter = 0;
    }
  }
  return false;
}
function checkMainDiag() {
  // чек главной диагонали
  for (let i = 0; i < FieldArray.length; i++) {
    if (FieldArray[i][i] !==Player) {
      return false
    }
  }
  // покраска
  for (let i = 0; i < FieldArray.length; i++) {
    renderSymbolInCell(Player, i, i, 'red');
  }
  return true;
}
function checkAntiDiag() {
  // чек побочной диагонали
  for (let i = 0; i < FieldArray.length; i++) {
    if(FieldArray[FieldArray.length - 1 - i][i]!==Player) {
      return false;
    }
  }
  // покраска
  for (let i = 0; i < FieldArray.length; i++) {
    renderSymbolInCell(Player, FieldArray.length - 1 - i, i, 'red');
  }
  return true;
}
function checkTurns () {
  
  return TurnCounter === DIMENSION*DIMENSION ? false : true;
}
function checkWinner() {
  if (checkTurns()) {
    if (checkRows() || checkColumns() || checkMainDiag() || checkAntiDiag()) {
      showMessage(`${Player} победил!`);
      return true
    }
    else {
      return false
    }
  }
  else {
    showMessage("Победила дружба");
    Game = 0;
    return true;
  }

}
function startGame() {
    do {
    DIMENSION = prompt("Сколько в поле будет строк?", '3');
    DIMENSION = Math.floor(Number(DIMENSION));    
    } while ( Number.isNaN(DIMENSION) || DIMENSION === 0)

    iniField();
    clearFieldArray();
    showMessage(`Сейчас ходит ${Player}`);
    renderGrid(DIMENSION);
}
function isCellEmpty(row, col) {
    
    if (FieldArray[row][col]===EMPTY) {
      return true;  
    }
    else {
      return false;
    }
    
}
function setCellState(symbol, row, col) {
  FieldArray[row][col] = symbol;
}
function changePlayer () {
  Player = Player === CROSS ? ZERO : CROSS;
  return Player; 
}
function cellClickHandler(row, col) {
  if (Game) {
    TurnCounter++;
    
    if (isCellEmpty(row, col)) {      
      setCellState(Player, row, col);
      renderSymbolInCell(Player, row, col);            
      
      if (checkWinner(Player)) {
        
        Game = 0;
      }
      else {
        changePlayer();
        showMessage(`Сейчас ходит ${Player}`);
      }
    }
    else {
      showMessage("Тут уже что-то есть :)");
    }
  }
  else {
    showMessage(`Чтобы начать заново, нажми "Сначала"`);
  } 
}
function clearFieldArray() {
  for (let i = 0; i < FieldArray.length; i++) {
    for (let j = 0; j < FieldArray.length; j++) {
      FieldArray[i][j] = EMPTY;
    }
  }
}
function resetClickHandler() {
    clearFieldArray();
    Player = CROSS;
    TurnCounter = 0;
    Game = 1;
    startGame();    
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function getAllRows() {
  document.querySelectorAll("tr");
}
function getAllElements() {
  document.querySelectorAll("td");
}
function showMessage(text) {
    var msg = document.querySelector(".message");
    msg.innerText = text;
}

/* Нарисовать игровое поле заданного размера */
function renderGrid(dimension) {
    var container = getContainer();
    container.innerHTML = "";

    for (let i = 0; i < dimension; i++) {
        var row = document.createElement("tr");
        for (let j = 0; j < dimension; j++) {
            var cell = document.createElement("td");
            cell.textContent = EMPTY;
            cell.addEventListener("click", () => cellClickHandler(i, j));
            row.appendChild(cell);
        }
        container.appendChild(row);
    }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell(symbol, row, col, color = "#333") {
    var targetCell = findCell(row, col);

    targetCell.textContent = symbol;
    targetCell.style.color = color;
}

function findCell(row, col) {
    var container = getContainer();
    var targetRow = container.querySelectorAll("tr")[row];
    return targetRow.querySelectorAll("td")[col];
}

function getContainer() {
    return document.getElementById("fieldWrapper");
}

/* Test Function */
/* Победа первого игрока */
function testWin() {
    clickOnCell(0, 2);
    clickOnCell(0, 0);
    clickOnCell(2, 0);
    clickOnCell(1, 1);
    clickOnCell(2, 2);
    clickOnCell(1, 2);
    clickOnCell(2, 1);
}

/* Ничья */
function testDraw() {
    clickOnCell(2, 0);
    clickOnCell(1, 0);
    clickOnCell(1, 1);
    clickOnCell(0, 0);
    clickOnCell(1, 2);
    clickOnCell(1, 2);
    clickOnCell(0, 2);
    clickOnCell(0, 1);
    clickOnCell(2, 1);
    clickOnCell(2, 2);
}

function clickOnCell(row, col) {
    findCell(row, col).click();
}
